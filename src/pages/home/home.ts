import { Component } from '@angular/core';
import { NavController ,ActionSheetController, AlertController,} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    customListes:{
        items:
            [{ name: string,check: boolean ,sub:string,hide:boolean}]
    }
    checkListes:[{
        category:string,
        items:
         [{ name: string,check: boolean ,sub:string,hide:boolean}]
    }]= [{
        category:'食品',
        items:[
            {name: '非常食', check: false , sub:'缶詰・お菓子など',hide:false},
            {name: '飲料水', check: false , sub:'',hide:false},
            ]
        },{
        category:'便利品',
        items:[
            {name: '防災頭巾・ヘルメット', check: false,sub:'',hide:false},
            {name: '懐中電灯', check: false,sub:'',hide:false},
            {name: '笛・ブザー', check: false,sub:'音を出して居場所を知らせるもの',hide:false},
            {name: '万能ナイフ', check: false,sub:'',hide:false},
            {name: 'マスク', check: false,sub:'',hide:false},
            {name: 'ビニール袋', check: false,sub:'大小あるとなお良し',hide:false},
            {name: 'アルミ性保温シート', check: false,sub:'',hide:false},
            {name: '毛布', check: false,sub:'',hide:false},
            {name: 'スリッパ', check: false,sub:'',hide:false},
            {name: '軍手', check: false,sub:'',hide:false},
            {name: 'マッチ・ライター', check: false,sub:'',hide:false},
            {name: '雨具', check: false,sub:'レインコートや長靴も',hide:false},
            {name: 'レジャーシート', check: false,sub:'',hide:false},
            {name: '簡易トイレ', check: false,sub:'',hide:false},
            ]
        },{
        category:'清潔・健康',
        items:[
            {name: '救急セット', check: false,sub:'',hide:false},
            {name: '常備薬・持病の薬', check: false,sub:'',hide:false},
            {name: 'タオル', check: false,sub:'',hide:false},
            {name: 'トイレットペーパー', check: false,sub:'',hide:false},
            {name: '着替え', check: false,sub:'下着も含む',hide:false},
            {name: 'ウェットティッシュ', check: false,sub:'大小あるとなお良し',hide:false},
            ]
        },{
        category:'情報収集',
        items:[
            {name: '携帯電話', check: false,sub:'充電器も忘れずに',hide:false},
            {name: '携帯ラジオ', check: false,sub:'予備の電池も忘れずに',hide:false},
            {name: '家族の写真', check: false,sub:'はぐれたときの確認用',hide:false},
            {name: '緊急時の家族・知人の連絡先', check: false,sub:'',hide:false},
            {name: '避難地図', check: false,sub:'避難場所の確認を先にしておきましょう。',hide:false},
            {name: '筆記用具', check: false,sub:'メモ帳も忘れずに',hide:false},
            ]
        },{
        category:'貴重品',
        items:[
            {name: '現金', check: false,sub:'公衆電話用に10円、100円とかも用意',hide:false},
            {name: '車・家の予備の鍵', check: false,sub:'',hide:false},
            {name: '予備のメガネ・コンタクト', check: false,sub:'',hide:false},
            {name: '銀行の口座番号・生命保険の契約番号など', check: false,sub:'',hide:false},
            {name: '健康保険証', check: false,sub:'',hide:false},
            {name: '身分証明書', check: false,sub:'運転免許証・パスポートなど',hide:false},
            {name: '印鑑', check: false,sub:'',hide:false},
            {name: '母子健康手帳', check: false,sub:'',hide:false},
            ]
        },{
        category:'その他',
        items:[
            {name: '紙おむつ', check: false,sub:'幼児用・高齢者用',hide:false},
            {name: '生理用品', check: false,sub:'',hide:false},
            {name: '粉ミルク・哺乳瓶', check: false,sub:'他あかちゃんに欠かせないもの',hide:false},
            {name: 'その他自分の生活に欠かせないもの', check: false,sub:'',hide:false},
            ]
        }
    ]
    checkList:boolean
    //alertCtrl:any

    //itemPatternes:any//チェック済みのリスト

  constructor(public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController) {
      if(localStorage.getItem('checkList')){
          //console.log('localStorageあり')
          this.checkListes = JSON.parse(localStorage.getItem('checkList'));
          this.customListes = JSON.parse(localStorage.getItem('customList'));
      }
  }

  //リスト削除
    listHide(s,i,name) {
        const prompt = this.alertCtrl.create({
            title: '削除確認',
            message: "本当に「" + name + "」を削除しますか？",
            buttons: [
                {
                    text: '削除しない',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: '削除する',
                    handler: data => {
                        console.log('Saved clicked');
                        this.checkListes[s].items[i].hide = true
                        localStorage.setItem('checkList', JSON.stringify(this.checkListes));
                    }
                }
            ]
        });
        prompt.present();
    }



    listSave(status,s,i) {//パンの種類を保存
        console.log('ステータス',status,s,i)

        console.log('ステータス前',this.checkListes[s].items[i].check)
        setTimeout(()=> {
            if (status == true){
                this.checkListes[s].items[i].check = false
            }else{
                this.checkListes[s].items[i].check = true
            }
            console.log('ステータス後',this.checkListes[s].items[i].check)
            localStorage.setItem('checkList', JSON.stringify(this.checkListes));
        }, 10)

    }

}
